

year = int(input("Enter year: "))

if year > 0:
	if(year%4 == 0 and year%100 !=0) or (year%400 == 0):
		print(year, "is a leap year")
	else:
		print(year, "is not a leap year")
else:
	print("No zero or negative values \n")




rows = int(input("Enter number of rows: "))
column = int(input("Enter number of columns: "))

for i in range(1, rows + 1):  
    for j in range(1, column + 1):  
        if j <= i:  
            print("*", end=' ')  
        else:  
            print("*", end=' ')  
    print()  

